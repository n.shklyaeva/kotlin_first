package homework8

val userCart = mutableMapOf<String, Int>(
    "potato" to 2,
    "cereal" to 2,
    "milk" to 1,
    "sugar" to 3,
    "onion" to 1,
    "tomato" to 2,
    "cucumber" to 2,
    "bread" to 3
)
val discountSet = setOf("milk", "bread", "sugar")
val discountValue = 0.20
val vegetableSet = setOf("potato", "tomato", "onion", "cucumber")
val prices = mutableMapOf<String, Double>(
    "potato" to 33.0,
    "sugar" to 67.5,
    "milk" to 58.7,
    "cereal" to 78.4,
    "onion" to 23.76,
    "tomato" to 88.0,
    "cucumber" to 68.4,
    "bread" to 22.0
)

fun main() {
    println(calculateVegetables(userCart))
    println(calculatePrice(userCart))
}

fun calculatePrice(userCart: MutableMap<String, Int>): Double {
    var price = 0.0
    var priceToCount = 0.0
    var totalPrice = 0.0
    for ((key, value) in userCart) {
        priceToCount = prices.getValue(key)
        price = if (discountSet.contains(key)) value * priceToCount * (1 - discountValue)
        else value * priceToCount
        totalPrice += price
    }
    return totalPrice
}

fun calculateVegetables(userCart: MutableMap<String, Int>): Int {
    var numberOfVegetables = 0
    for ((key, value) in userCart) {
        if (vegetableSet.contains(key)) numberOfVegetables += value
    }
    return numberOfVegetables
}

