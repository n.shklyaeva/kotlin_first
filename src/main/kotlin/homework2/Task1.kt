package homework2

fun main() {

    val a : Byte = 50
    val b : Short = 200
    val c : Long = 3000000000
    val d : Double = 0.666666667
    val e : Int = 18500
    val f : Float = 0.5F
    val g : String = "Что-то авторское!"

    val ml = "мл"
    val drops = "капель"
    val liter = "литра"

    println("Заказ - '$e $ml лимонада' готов!")
    println("Заказ - '$b $ml пина колады' готов!")
    println("Заказ - '$a $ml виски' готов!")
    println("Заказ - '$c $drops фреша' готов!")
    println("Заказ - '$f $liter колы' готов!")
    println("Заказ - '$d $liter эля' готов!")
    println("Заказ - '$g' готов!")
}