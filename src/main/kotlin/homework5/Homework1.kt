package homework5

fun main() {
    val scar = Lion("Шрам", 2.1, 190.5)

    val shereKhan = Tiger("Шерхан", 1.15, 230.5)

    val gloria = Hippo("Глория", 1.65, 3500.5)

    val greyWolf = Wolf("Серый волк", 0.85, 100.0)

    val melman = Giraffe("Мелман", 5.6, 1000.3)

    val dumbo = Elephant("Дамбо", 3.6, 4500.8)

    val abu = Chimpanzee("Абу", 1.5, 53.0)

    val gorilla = Gorilla("Горилла", 2.5, 84.0)

    val animals = arrayOf(scar, shereKhan, gloria, greyWolf, melman, dumbo, abu, gorilla)
    val food = arrayOf("Мясо", "Зебра", "Антилопа", "Трава", "Листья", "Фрукт", "Трава", "Банан")

    everyoneIsFeeding(animals, food)
}

abstract class Animal(open val name: String, open val height: Double, open val weight: Double) {
    abstract val favouriteFood: Array<String>
    var satiety: Int = 0

    fun eat(food: String) {
        if (favouriteFood.contains(food)) satiety++
    }
}

fun everyoneIsFeeding(animals: Array<Animal>, favouriteFood: Array<String>) {
    for (animal in animals) {
        for (food in favouriteFood) {
            animal.eat(food)
        }
        println("Сытость ${animal.name} = ${animal.satiety}")
    }
}

class Lion(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val favouriteFood: Array<String> = arrayOf("Мясо", "Зебра", "Антилопа")
}

class Tiger(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val favouriteFood: Array<String> = arrayOf("Мясо", "Косуля", "Олень")
}

class Hippo(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val favouriteFood: Array<String> = arrayOf("Трава", "Овощ", "Фрукт")
}

class Wolf(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val favouriteFood: Array<String> = arrayOf("Мясо", "Овца", "Корова")
}

class Giraffe(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val favouriteFood: Array<String> = arrayOf("Трава", "Листья", "Фрукт")
}

class Elephant(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val favouriteFood: Array<String> = arrayOf("Яблоко", "Банан", "Куст")
}

class Chimpanzee(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val favouriteFood: Array<String> = arrayOf("Орешки", "Банан", "Термиты")
}

class Gorilla(name: String, height: Double, weight: Double) : Animal(name, height, weight) {

    override val favouriteFood: Array<String> = arrayOf("Бамбук", "Банан", "Плоды")
}