package homework5

fun main() {
    val digit = readLine()!!.toInt()
    println(reverse(digit))
}

fun reverse(yourNumber: Int): Int {
    var number = yourNumber
    var result = 0
    while (number != 0) {
        val digit = number % 10
        number /= 10
        result = result * 10 + digit
    }
    return result
}
