package battleship

fun main() {
    println("Начинаем игру! Поле - 4 на 4. Нумерация строк от 0 до 3 (сверху вниз по оси x). Нумерация столбцов от 0 до 3 (слева направо по оси y). Каждому игроку нужно расположить по одному 2-хпалубному и по два 1-палубных корабля!")
    println("Введите имя первого игрока!")
    val player1Name = readLine().toString()
    val player1TwoDeckShip = TwoDeckShip()
    val player1OneDeckShip1 = OneDeckShip()
    val player1OneDeckShip2 = OneDeckShip()
    val player1 = Player(
        player1Name,
        player1TwoDeckShip.coordinates(),
        player1OneDeckShip1.coordinates(),
        player1OneDeckShip2.coordinates()
    )

    val tableShipSet1 = player1.shipSet()
    println("Введите имя второго игрока!")
    val player2Name = readLine().toString()
    val player2TwoDeckShip = TwoDeckShip()
    val player2OneDeckShip1 = OneDeckShip()
    val player2OneDeckShip2 = OneDeckShip()
    val player2 = Player(
        player2Name,
        player2TwoDeckShip.coordinates(),
        player2OneDeckShip1.coordinates(),
        player2OneDeckShip2.coordinates()
    )

    val tableShipSet2 = player2.shipSet()
    while (shooting(player1, tableShipSet2) < 4) {
        if (shooting(player2, tableShipSet1) == 4) break
    }
}

class Player(
    val name: String,
    private val twoDeckShip: Array<Int>,
    private val oneDeckShip1: Array<Int>,
    private val oneDeckShip2: Array<Int>
) {
    var tableShipSet: Array<Array<Int>> = Array(4) { Array(4) { 0 } }
    fun shipSet(): Array<Array<Int>> {
        tableShipSet[twoDeckShip[0]][twoDeckShip[1]] = 2
        tableShipSet[twoDeckShip[2]][twoDeckShip[3]] = 2
        tableShipSet[oneDeckShip1[0]][oneDeckShip1[1]] = 1
        tableShipSet[oneDeckShip2[0]][oneDeckShip2[1]] = 1
        println("Расстановка кораблей игрока $name!")
        for (row in tableShipSet) {
            for (cell in row) {
                print("$cell \t")
            }
            println()
        }
        return tableShipSet
    }
}

class OneDeckShip() {
    fun coordinates(): Array<Int> {
        println("Введите номер строки (координату x от 0 до 3) 1-палубного корабля")
        val xOneDeckShip = readLine()?.toInt()!!
        println("Введите номер столбца (координату y от 0 до 3) 1-палубного корабля")
        val yOneDeckShip = readLine()?.toInt()!!
        return arrayOf(xOneDeckShip, yOneDeckShip)
    }
}

class TwoDeckShip() {
    fun coordinates(): Array<Int> {
        println("Введите номер строки (координату x от 0 до 3) начала 2-хпалубного корабля")
        val x1TwoDeckShip = readLine()?.toInt()!!
        println("Введите номер столбца (координату y от 0 до 3) начала 2-хпалубного корабля")
        val y1TwoDeckShip = readLine()?.toInt()!!
        println("Введите номер строки (координату x от 0 до 3) конца 2-хпалубного корабля")
        val x2TwoDeckShip = readLine()?.toInt()!!
        println("Введите номер столбца (координату y от 0 до 3) конца 2-хпалубного корабля")
        val y2TwoDeckShip = readLine()?.toInt()!!
        return arrayOf(x1TwoDeckShip, y1TwoDeckShip, x2TwoDeckShip, y2TwoDeckShip)
    }
}

fun shooting(player: Player, battleField: Array<Array<Int>>): Int {
    var shootAgain = true
    var numberOfHits = 0
    for (row in battleField) {
        for (cell in row) {
            if (cell == 4) numberOfHits++
        }
    }
    while (numberOfHits < 4) {
        println("Стреляет игрок ${player.name}!")
        println("Введите координату x выстрела (от 0 до 3)!")
        val x = try {
            readLine()?.toInt()
        } catch (e: NumberFormatException) {
            println("Неверное значение! Введите цифру от 0 до 3!")
            continue
        }
        println("Введите координату y выстрела (от 0 до 3)!")
        val y = try {
            readLine()?.toInt()
        } catch (e: NumberFormatException) {
            println("Неверное значение! Введите цифру от 0 до 3!")
            continue
        }
        try {
            when (battleField[x!!][y!!]) {
                1 -> {
                    println("Убил!")
                    battleField[x][y] = 4
                    numberOfHits++
                }

                2 -> {
                    println("Попал!")
                    battleField[x][y] = 4
                    numberOfHits++
                }

                0 -> {
                    println("Мимо!")
                    battleField[x][y] = 3
                    shootAgain = false
                }

                3, 4 -> println("Сюда выстрел уже сделан! Сделай другой ход!")
            }

        } catch (e: IndexOutOfBoundsException) {
            println("Введены неверные координаты! Стреляй еще раз внимательно!")
        }
        for (row in battleField) {
            for (cell in row) {
                print("$cell \t")
            }
            println()
        }
        if (numberOfHits == 4) println("Игра окончена! Победил ${player.name}!")
        if (numberOfHits == 4 || !shootAgain)
            break
    }
    return numberOfHits
}