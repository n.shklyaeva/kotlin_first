package homework4

fun main() {
    val myArray = arrayOf(1, -1, -2, 4, 7, 10, 0, 19, -27)

    for (item in myArray) {
        var isSwapped = false
        for (j in 0 until myArray.size - 1) {
            if (myArray[j] > myArray[j + 1]) {
                val swap = myArray[j]
                myArray[j] = myArray[j + 1]
                myArray[j + 1] = swap
                isSwapped = true
            }
        }
        if (!isSwapped) break
    }

    for (item in myArray) {
        print("$item ")
    }
}
