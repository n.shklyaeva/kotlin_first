package homework4

fun main() {
    val marks = arrayOf(3, 4, 5, 2, 3, 5, 5, 2, 4, 5, 2, 4, 5, 3, 4, 3, 3, 4, 4, 5)
    var excellentStudents = 0.0
    var goodMarkStudents = 0.0
    var threeMarkStudents = 0.0
    var poorMarkStudents = 0.0
    for (item in marks) {
        when (item) {
            2 -> poorMarkStudents++
            3 -> threeMarkStudents++
            4 -> goodMarkStudents++
            5 -> excellentStudents++
        }
    }
    println("Отличников - ${percent(excellentStudents, marks.size)}%")
    println("Хорошистов - ${percent(goodMarkStudents, marks.size)}%")
    println("Троечников - ${percent(threeMarkStudents, marks.size)}%")
    println("Двоечников - ${percent(poorMarkStudents, marks.size)}%")
}

fun percent(studentMark: Double, numberOfStudents: Int): Double {
    return studentMark / numberOfStudents * 100
}
