package homework4

fun main() {
    val myArray = arrayOf(1, 25, 10, 6, 22, 17, 8, 14, 23)
    var captainJohnCoins = 0
    var crewCoins = 0
    for (item in myArray) {
        if (item % 2 == 0) captainJohnCoins++ else crewCoins++
    }
    println("Количество монет, полученных капитаном Джо -  $captainJohnCoins")
    println("Количество монет, полученных командой -  $crewCoins")
}
