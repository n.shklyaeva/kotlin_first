package homework7

fun main() {
    val user = createUser(readln(), readln(), readln())
    println("${user.login} - ${user.password}")
}

fun createUser(login: String, password: String, confirmPassword: String): User {
    if (login.length > 20) {
        throw WrongLoginException("Логин не должен быть длиннее 20 символов")
    }
    if (password.length < 10 || password != confirmPassword) {
        throw WrongPasswordException("Пароль не соответствует условиям")
    }
    return User(login, password)
}

class User(val login: String, val password: String)

class WrongLoginException(message: String) : Exception(message)

class WrongPasswordException(message: String) : Exception(message)
